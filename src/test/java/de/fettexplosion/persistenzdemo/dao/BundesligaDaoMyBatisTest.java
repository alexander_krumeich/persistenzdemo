package de.fettexplosion.persistenzdemo.dao;

import static de.fettexplosion.persistenzdemo.BundesligaTestWelt.STPAULI_NAME;
import static de.fettexplosion.persistenzdemo.BundesligaTestWelt.alleStPauliSpiele;
import static de.fettexplosion.persistenzdemo.BundesligaTestWelt.fcStPauli;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.dao.BundesligaDaoMyBatis;
import de.fettexplosion.persistenzdemo.mapper.spielausgang.SpielausgangMapper;
import de.fettexplosion.persistenzdemo.mapper.vereinsname.VereinsnameMapper;

/**
 * Beispiel, wie sich die MyBatis Klassen 
 * in einem Unit-Test mocken lassen.
 * 
 * Hier ist nur eine Testmethode des DAOs implementiert.
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class BundesligaDaoMyBatisTest {

	public static final String KUERZEL_ST_PAULI = "%St. Pauli%";

	@Mock protected SqlSessionFactory mockSqlSessionFactory;
	@Mock protected SqlSession mockSqlSession;
	@Mock protected VereinsnameMapper mockVereinsnameMapper;
	@Mock protected SpielausgangMapper mockSpielausgangMapper;
	
	
	@Before
	public void prepareSqlSessionFactory() {
		when(mockSqlSessionFactory.openSession()).thenReturn(mockSqlSession);
	}
	
	@Test
	public void vereinsnameWirdGelesen() throws Exception {
		when(mockVereinsnameMapper.readTeam(eq(KUERZEL_ST_PAULI))).thenReturn(fcStPauli);
		when(mockSqlSession.getMapper(VereinsnameMapper.class)).thenReturn(mockVereinsnameMapper);
		BundesligaDaoMyBatis blDao = new BundesligaDaoMyBatis();
		blDao.sqlSessionFactory = mockSqlSessionFactory;
		Team teamFromDb = blDao.readTeam(KUERZEL_ST_PAULI);
		assertEquals(STPAULI_NAME, teamFromDb.getName());
	}

	@Test(expected=RuntimeException.class)
	public void sqlExceptionBeimZugriffWirdAlsRuntimeExceptionGeworfen() {
		doThrow(new SQLException()).when(mockVereinsnameMapper).readTeam(eq(KUERZEL_ST_PAULI));
		when(mockSqlSession.getMapper(VereinsnameMapper.class)).thenReturn(mockVereinsnameMapper);
		BundesligaDaoMyBatis blDao = new BundesligaDaoMyBatis();
		blDao.sqlSessionFactory = mockSqlSessionFactory;
		blDao.readTeam(KUERZEL_ST_PAULI);
	}
	
	@Test
	public void alleStPauliHeimspieleWerdenGelesen() throws Exception {
		when(mockSpielausgangMapper.readErgebnisseForTeam(eq(fcStPauli))).thenReturn(alleStPauliSpiele);
		when(mockSqlSession.getMapper(SpielausgangMapper.class)).thenReturn(mockSpielausgangMapper);
		BundesligaDaoMyBatis blDao = new BundesligaDaoMyBatis();
		blDao.sqlSessionFactory = mockSqlSessionFactory;
		List<Partie> alleSpieleFromDb = blDao.readErgebnisseBySpieltagFuerTeam(fcStPauli);
		assertEquals(alleStPauliSpiele, alleSpieleFromDb);
		
	}

}