package de.fettexplosion.persistenzdemo.bm.tabelle;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.tabelle.Tabelle;
import de.fettexplosion.persistenzdemo.bm.tabelle.Tabelleneintrag;
import de.fettexplosion.persistenzdemo.openliga.OpenLigaDao;
import de.fettexplosion.persistenzdemo.results.ResultsProvider;

public class TabelleTest {

	@Test
	public void tabelleWirdInitialisiert() {
		Tabelle t = new Tabelle();
		for (Tabelleneintrag eintrag : t) {
			assertEquals(0, eintrag.getPunkte());
			assertEquals(0, eintrag.getTore());
			assertEquals(0, eintrag.getGegentore());
		}
	}
	
	@Test
	public void tabelleWirdRichtigBefuellt() throws Exception {
		Tabelle t = new Tabelle();
		ResultsProvider r = new OpenLigaDao();
		List<Partie> ergebnisse = r.getAllPartien();
		for (Partie partie : ergebnisse) {
			t.addResult(partie);
		}
		for (Tabelleneintrag tabelleneintrag : t) {
			System.out.println(tabelleneintrag);
		}
		Tabelleneintrag stPauli = t.getTabellenplatz(8);
		assertEquals("FC St. Pauli", stPauli.getTeam().getName());
		assertEquals(48, stPauli.getPunkte());
		assertEquals(44, stPauli.getTore());
		assertEquals(49, stPauli.getGegentore());
		
	}

}
