package de.fettexplosion.persistenzdemo.bm.tabelle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fettexplosion.persistenzdemo.bm.tabelle.Tabelleneintrag;

public class TabelleneintragTest {
	
	private void checkResults(Tabelleneintrag t, int punkte, int tore, int gegentore) {
		assertEquals(punkte, t.getPunkte());
		assertEquals(tore, t.getTore());
		assertEquals(gegentore, t.getGegentore());
	}

	@Test
	public void leererTabelleneintragWirdErstellt() {
		Tabelleneintrag t = new Tabelleneintrag();
		assertEquals(0, t.getPunkte());
		assertEquals(0, t.getTore());
		assertEquals(0, t.getGegentore());
	}
	
	@Test
	public void testVergleichMitPunkten1() throws Exception {
		Tabelleneintrag t1 = new Tabelleneintrag();
		Tabelleneintrag t2 = new Tabelleneintrag();
		t1.addSieg();
		t2.addUnentschieden();
		assertEquals(-1, t1.compareTo(t2));
		assertEquals(1, t2.compareTo(t1));
	}

	@Test
	public void testVergleichMitPunkten2() throws Exception {
		Tabelleneintrag t1 = new Tabelleneintrag();
		Tabelleneintrag t2 = new Tabelleneintrag();
		t1.addSieg();
		t2.addSieg();
		assertEquals(0, t1.compareTo(t2));
		assertTrue(t1.equals(t2));
	}

	@Test
	public void testVergleichMitToren1() throws Exception {
		Tabelleneintrag t1 = new Tabelleneintrag();
		Tabelleneintrag t2 = new Tabelleneintrag();
		t1.addTore(2);
		t2.addGegentore(1);
		assertEquals(-1, t1.compareTo(t2));
		assertEquals(1, t2.compareTo(t1));
	}
	
	@Test
	public void testVergleichMitToren2() throws Exception {
		Tabelleneintrag t1 = new Tabelleneintrag();
		Tabelleneintrag t2 = new Tabelleneintrag();
		t1.addTore(4);
		t2.addTore(4);
		assertEquals(0, t1.compareTo(t2));
		assertTrue(t1.equals(t2));
	}

	@Test
	public void toreWerdenAddiert() {
		Tabelleneintrag t = new Tabelleneintrag();
		t.addTore(5);
		checkResults(t, 0, 5, 0);
	}

	@Test
	public void gegentoreWerdenAddiert() {
		Tabelleneintrag t = new Tabelleneintrag();
		t.addGegentore(5);
		checkResults(t, 0, 0, 5);
	}

	@Test
	public void punkteWerdenGesetztFuerUnentschieden() {
		Tabelleneintrag t = new Tabelleneintrag();
		t.addUnentschieden();
		checkResults(t, 1, 0, 0);
	}

	@Test
	public void punkteWerdenGesetztFuerSieg() {
		Tabelleneintrag t = new Tabelleneintrag();
		t.addSieg();
		checkResults(t, 3, 0, 0);
	}
}
