package de.fettexplosion.persistenzdemo;

import java.util.ArrayList;
import java.util.List;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.dao.BundesligaDao;
import de.fettexplosion.persistenzdemo.openliga.OpenLigaDao;
import de.fettexplosion.persistenzdemo.results.ResultsProvider;

/**
 * 
 */
public class BundesligaLasttest {
	
	private static final int MAX_WARMUP_ITERATIONS = 10000;
	private static final int MAX_ITERATIONS = 1000000;

	private static final String DEFAULT_DAO_CLASS = "de.fettexplosion.persistenzdemo.dao.BundesligaDaoMyBatisSpring";

	BundesligaDao blDao;

	public static void main(String[] args) throws Exception {
		String theDaoClazzName = args.length >= 1 ? args[0] : DEFAULT_DAO_CLASS;
		BundesligaLasttest demo = new BundesligaLasttest(theDaoClazzName);
		demo.runTest();
	}

	public BundesligaLasttest(String theDaoClassName) throws Exception  {
		Class<?> theDaoClazz = Class.forName(theDaoClassName);
		blDao = (BundesligaDao) theDaoClazz.newInstance();
	}

	private void runTest() throws InterruptedException {
		ResultsProvider r = new OpenLigaDao();
		List<Team> alleTeams = r.getAllTeams();
		List<Partie> allePartien = r.getAllPartien();
		blDao.populateDb(alleTeams, allePartien);
		Thread.sleep(3000);
		System.out.println("Running with " + blDao.getClass());
		System.out.println(String.format("Dauer der Aufwärmphase: %,d msec (%,d Iterationen)", run(MAX_WARMUP_ITERATIONS)/1000000, MAX_WARMUP_ITERATIONS));
		System.gc();
		Thread.sleep(10000);
		System.out.println(String.format("Dauer des Tests: %,d msec (%,d Iterationen)", run(MAX_ITERATIONS)/1000000, MAX_ITERATIONS));
		
	}


	@SuppressWarnings("unused")
	private long run(int iterations) {
		Team team;
		List<Partie> heimsiege = new ArrayList<>();
		List<Team> allTeams = new ArrayList<>();
		Partie auftaktPartie = new Partie();
		List<Partie> ergebnisseStPauli = new ArrayList<>();;
		long start = System.nanoTime();
		for (int i = 0; i < iterations; i++) {
			heimsiege = blDao.readHeimsiegeBySpieltag();
			Team firstWinner = heimsiege.get(3).getHeim();
			allTeams = blDao.readAllTeams();
			team = blDao.readTeam("FC St. Pauli");
			auftaktPartie = blDao.readAuftaktPartie(firstWinner);
			ergebnisseStPauli = blDao.readErgebnisseBySpieltagFuerTeam(firstWinner);
		}
		long end = System.nanoTime();
		return end - start;
	}

}






