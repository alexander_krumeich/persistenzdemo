package de.fettexplosion.persistenzdemo.bm.tabelle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.openliga.OpenLigaDao;
import de.fettexplosion.persistenzdemo.results.ResultsProvider;

public class Tabelle implements Iterable<Tabelleneintrag>{
	
	private List<Tabelleneintrag> tabelle;
	private Map<Team, Tabelleneintrag> eintragByTeam;
	private Set<Partie> beruecksichtigePartien;
	
	private ResultsProvider resultsProvider = new OpenLigaDao();
	
	
	public Tabelle() {
		tabelle = new ArrayList<>();
		beruecksichtigePartien = new HashSet<>();
		eintragByTeam = new HashMap<>();
		for (Team t : resultsProvider.getAllTeams()) {
			Tabelleneintrag tabellenEintrag = new Tabelleneintrag();
			tabellenEintrag.setTeam(t);
			tabelle.add(tabellenEintrag);
			eintragByTeam.put(t, tabellenEintrag);
		}
	}
	
	public Tabelleneintrag getTabellenplatz(int i) {
		prepareTabelle();
		return tabelle.get(i-1);
	}
	
	public void addResult(Partie p) {
		if (beruecksichtigePartien.contains(p)) {
			throw new IllegalStateException();
		} 
		beruecksichtigePartien.add(p);
		Tabelleneintrag heim = eintragByTeam.get(p.getHeim());
		Tabelleneintrag gast = eintragByTeam.get(p.getGast());
		heim.addTore(p.getToreHeim());
		heim.addGegentore(p.getToreGast());
		gast.addTore(p.getToreGast());
		gast.addGegentore(p.getToreHeim());
		if (p.getErgebnis() == Ergebnis.heimsieg) {
			heim.addSieg();
		} else if (p.getErgebnis() == Ergebnis.auswärtssieg) {
			gast.addSieg();
		} else {
			heim.addUnentschieden();
			gast.addUnentschieden();
		}
	}
	
	private void prepareTabelle() {
		Collections.sort(tabelle);
		int tabellenplatz=1;
		for (Tabelleneintrag t : tabelle) {
			t.setTabellenplatz(tabellenplatz++);
		}
	}
	
	@Override
	public Iterator<Tabelleneintrag> iterator() {
		prepareTabelle();
		return tabelle.iterator();
	}
	

}
