package de.fettexplosion.persistenzdemo.bm.tabelle;

import de.fettexplosion.persistenzdemo.bm.Team;

public class Tabelleneintrag implements Comparable<Tabelleneintrag>{
	
	private static final int PUNKTE_FUER_SIEG = 3;
	private static final int PUNKTE_FUER_UNENTSCHIEDEN = 1;
	
	private Team team;
	private int punkte;
	private int tore;
	private int gegentore;
	
	private int tabellenplatz;
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Tabelleneintrag)) {
			return false;
		} else {
			Tabelleneintrag other = (Tabelleneintrag) obj;
			return (other.getPunkte() == punkte) && (other.getTore() == tore) && (other.getGegentore() == gegentore); 
		}
	}
	
	@Override
	public int compareTo(Tabelleneintrag o) {
		if (punkte > o.getPunkte()) {
			return -1;
		} else if (punkte < o.getPunkte()) {
			return 1;
		} else if (getTordifferenz() > o.getTordifferenz()) {
			return -1;
		} else if (getTordifferenz() < o.getTordifferenz()) {
			return 1;
		} else if (getTore() > o.getTore()) {
			return -1;
		} else if (getTore() < o.getTore()) {
			return 1;
		}
		return 0;
	}
	
	public void addTore(int i) {
		tore = tore + i;
	}
	public void addGegentore(int i) {
		gegentore = gegentore + i;
	}
	
	public void addSieg() {
		punkte = punkte + PUNKTE_FUER_SIEG;
	}
	
	public void addUnentschieden() {
		punkte = punkte + PUNKTE_FUER_UNENTSCHIEDEN;
	}
	
	private int getTordifferenz() {
		return tore - gegentore;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public int getPunkte() {
		return punkte;
	}

	public void setPunkte(int punkte) {
		this.punkte = punkte;
	}

	public int getTore() {
		return tore;
	}

	public void setTore(int tore) {
		this.tore = tore;
	}

	public int getGegentore() {
		return gegentore;
	}

	public void setGegentore(int gegentore) {
		this.gegentore = gegentore;
	}
	
	public int getTabellenplatz() {
		return tabellenplatz;
	}
	
	public void setTabellenplatz(int tabellenplatz) {
		this.tabellenplatz = tabellenplatz;
	}

	@Override
	public String toString() {
		return String.format("%2d  %-30s\t%2d:%2d\t%3d\t%3d",tabellenplatz, team.getName(), tore, gegentore, getTordifferenz(), punkte);
	}

	
	

}
