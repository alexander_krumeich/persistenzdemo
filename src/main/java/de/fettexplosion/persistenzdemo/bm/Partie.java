package de.fettexplosion.persistenzdemo.bm;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 
 */

@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Partie implements Serializable {
	
	private static final long serialVersionUID = 3620056360571003086L;
	
	private int id;
	private Team heim;
	private Team gast;
	private Ergebnis ergebnis;
	private int spieltag;
	private int toreHeim;
	private int toreGast;
	
	public Partie() {
	}

	public Partie(int id, int Spieltag, Team heim, Team gast, Ergebnis ergebnis, int toreHeim, int toreGast) {
		setId(id);
		setHeim(heim);
		setGast(gast);
		setErgebnis(ergebnis);
		setToreHeim(toreHeim);
		setToreGast(toreGast);
		setSpieltag(Spieltag);
	}
	
	@Basic
	public int getToreHeim() {
		return toreHeim;
	}
	public void setToreHeim(int toreHeim) {
		this.toreHeim = toreHeim;
	}
	
	@Basic
	public int getToreGast() {
		return toreGast;
	}
	public void setToreGast(int toreGast) {
		this.toreGast = toreGast;
	}

	@Basic
	public int getSpieltag() {
		return spieltag;
	}
	public void setSpieltag(int spieltag) {
		this.spieltag = spieltag;
	}
	
	@Basic
	public Team getHeim() {
		return heim;
	}
	public void setHeim(Team heim) {
		this.heim = heim;
	}

	@Basic
	public Team getGast() {
		return gast;
	}
	public void setGast(Team gast) {
		this.gast = gast;
	}
	
	
	@Basic
	public Ergebnis getErgebnis() {
		return ergebnis;
	}
	public void setErgebnis(Ergebnis ergebnis) {
		this.ergebnis = ergebnis;
	}

	@Id
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return String.format("%s : %s\t\t%d:%d (%s)", heim.getName(), gast.getName(), toreHeim, toreGast, ergebnis);
	}

}
