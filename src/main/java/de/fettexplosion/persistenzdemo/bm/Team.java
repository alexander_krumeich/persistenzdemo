package de.fettexplosion.persistenzdemo.bm;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 
 */
@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public class Team implements Serializable {
	
	private static final long serialVersionUID = -7972197302068318990L;

	private int id;
	private String name;
	
	public Team() {
	}
	
	public Team(int id, String name) {
		setId(id);
		setName(name);
	}
	
	@Id
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Basic
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
