package de.fettexplosion.persistenzdemo.bm;

public enum Ergebnis {

	unentschieden, heimsieg, auswärtssieg;
}
