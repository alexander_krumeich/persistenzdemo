package de.fettexplosion.persistenzdemo.dao;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.openliga.OpenLigaDao;
import de.fettexplosion.persistenzdemo.results.ResultsProvider;

public class BundesligaFunctional implements BundesligaDao {
	
	List<Team> alleTeams;
	List<Partie> allePartien;
	
	@Override
	public Team readTeam(String kuerzel) {
		Stream<Team> s = alleTeams.stream();
		Stream<Team> filtered = s.filter(t -> t.getName().toUpperCase().matches(kuerzel.toUpperCase()));
		return filtered.findFirst().get();
	}

	@Override
	public List<Team> readAllTeams() {
		return alleTeams.stream().collect(Collectors.toList());
	}

	@Override
	public Partie readAuftaktPartie(Team t) {
		return allePartien.stream()
				.filter(p -> (p.getSpieltag() == 1))
				.filter(p -> p.getHeim().equals(t) || p.getGast().equals(t))
				.findFirst().get();
	}

	@Override
	public List<Partie> readErgebnisseBySpieltagFuerTeam(Team t) {
		return allePartien.stream()
				.filter(p -> p.getHeim().equals(t) || p.getGast().equals(t))
				.collect(Collectors.toList());
	}

	@Override
	public List<Partie> readHeimsiegeBySpieltag() {
		return allePartien.stream()
				.filter(p -> p.getErgebnis().equals(Ergebnis.heimsieg))
				.collect(Collectors.toList());
	}

	@Override
	public List<Partie> readAllPartien() {
		return allePartien.stream().collect(Collectors.toList());
	}

	@Override
	public Set<Integer> readSpieltageFuerPaarung(Team team1, Team team2) {
		Predicate<Partie> isBegegnung = p -> (p.getHeim().equals(team1) && p.getGast().equals(team2)) || (p.getHeim().equals(team2) && p.getGast().equals(team1));  
		Set<Integer> spieltage = allePartien.stream()
				.filter(isBegegnung)
				.map(p -> p.getSpieltag())
				.collect(Collectors.toSet());
		return spieltage;
	}

	@Override
	public void populateDb(List<Team> alleTeams, List<Partie> allePartien) {
		ResultsProvider r = new OpenLigaDao();
		this.alleTeams = r.getAllTeams();
		this.allePartien = r.getAllPartien();
	}
	
	

}
