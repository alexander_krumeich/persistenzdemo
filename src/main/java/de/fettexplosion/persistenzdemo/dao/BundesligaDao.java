package de.fettexplosion.persistenzdemo.dao;

import java.util.List;
import java.util.Set;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public interface BundesligaDao {
	
	void populateDb(List<Team> alleTeams, List<Partie> allePartien);
	
	Team readTeam(String kuerzel);

	List<Team> readAllTeams();
	
	Partie readAuftaktPartie(Team t);

	List<Partie> readErgebnisseBySpieltagFuerTeam(Team t);
	
	List<Partie> readHeimsiegeBySpieltag();
	
	List<Partie> readAllPartien();
	
	default Set<Integer> readSpieltageFuerPaarung(Team team1, Team team2) {
		throw new RuntimeException("Not yet implemented");
	}
}
