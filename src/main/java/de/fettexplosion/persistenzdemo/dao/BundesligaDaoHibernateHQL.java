package de.fettexplosion.persistenzdemo.dao;

import java.util.List;

import org.hibernate.Session;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public class BundesligaDaoHibernateHQL extends BundesligaDaoCriteria {

	@Override
	public Team readTeam(String kuerzel) {
		Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
		List<Team> result = session.createQuery("FROM de.fettexplosion.persistenzdemo.bm.Team t WHERE t.name like :name").setString("name", kuerzel).list();
        session.close();
        return result.get(0); 
	}

	@Override
	public List<Team> readAllTeams() {
		Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
		List<Team> result = session.createQuery("FROM de.fettexplosion.persistenzdemo.bm.Team" ).list();
        session.close();
        return result;
	}

	@Override
	public Partie readAuftaktPartie(Team t) {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Partie> result = (List<Partie>) session.createQuery("from de.fettexplosion.persistenzdemo.bm.Partie p where p.spieltag = 1 and (p.heim = :t or p.gast = :t)").setParameter("t", t).list();
		session.close();
		return result.get(0); 
	}


	@Override
	public List<Partie> readErgebnisseBySpieltagFuerTeam(Team t) {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Partie> result = (List<Partie>) session.createQuery("from de.fettexplosion.persistenzdemo.bm.Partie p where (p.heim = :t or p.gast = :t)").setParameter("t", t).list();
		session.close();
		return result; 
	}

	@Override
	public List<Partie> readHeimsiegeBySpieltag() {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Partie> result = (List<Partie>) session.createQuery("from de.fettexplosion.persistenzdemo.bm.Partie p where p.ergebnis = :heimsieg").setParameter("heimsieg", Ergebnis.heimsieg).list();
		session.close();
		return result; 
	}
	
	@Override
	public List<Partie> readAllPartien() {
		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Partie> result = (List<Partie>) session.createQuery("from de.fettexplosion.persistenzdemo.bm.Partie").list();
		session.close();
		return result; 
	}

}
