package de.fettexplosion.persistenzdemo.dao;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public class BundesligaDaoJdbc implements BundesligaDao {
	
	private static final String INSERT_TEAM = "INSERT INTO teams (TEAM_ID, TEAM) VALUES (?,?);";
	private static final String INSERT_PARTIE = "INSERT INTO partien (PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (?,?,?,?,?,?,?);";
	
	private static final String SELECT_ALL_PARTIEN = "select P.SPIELTAG, H.TEAM AS HEIM, G.TEAM AS GAST, P.TORE_HEIM, P.TORE_GAST, P.ERGEBNIS from teams AS H, teams AS G, partien AS P where H.TEAM_ID=P.HEIM AND G.TEAM_ID=P.GAST order by P.SPIELTAG";
	private static final String SELECT_HEIMSIEGE = "select P.SPIELTAG, H.TEAM AS HEIM, G.TEAM AS GAST, P.TORE_HEIM, P.TORE_GAST, P.ERGEBNIS from teams AS H, teams AS G, partien AS P where P.ERGEBNIS='1' AND H.TEAM_ID=P.HEIM AND G.TEAM_ID=P.GAST order by P.SPIELTAG";
	private static final String SELECT_TEAM_BY_KUERZEL = "select TEAM_ID, TEAM from teams WHERE TEAM LIKE ?";
	private static final String SELECT_AUFTAKTPARTIE = "select P.HEIM as HEIM_ID, P.GAST as GAST_ID, P.ERGEBNIS as ergebnis, P.TORE_HEIM, P.TORE_GAST, H.TEAM as heim, G.TEAM as gast from partien as P left outer join teams H on H.TEAM_ID=P.HEIM left outer join teams G on G.TEAM_ID=P.GAST where P.SPIELTAG='1' and (P.HEIM=? or P.GAST=?) ";
	private static final String SELECT_TEAM_FROM_BL_TEAMS = "select TEAM_ID, TEAM from teams";
	private static final String SELECT_HEIMSIEGE_FOR_TEAM = "select P.HEIM as HEIM_ID, P.GAST as GAST_ID, P.ERGEBNIS as ergebnis, P.SPIELTAG, P.TORE_HEIM, P.TORE_GAST, H.TEAM as heim, G.TEAM as gast from partien as P left outer join teams H on H.TEAM_ID=P.HEIM left outer join teams G on G.TEAM_ID=P.GAST where P.HEIM=? or P.GAST=?";

	private static final String JDBC_URL = "jdbc:h2:~/test;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090;INIT=CREATE SCHEMA IF NOT EXISTS BL\\;SET SCHEMA BL\\;runscript from 'classpath:createTables.sql'";
	
	
	
	ComboPooledDataSource cpds = new ComboPooledDataSource();
	
	public BundesligaDaoJdbc() {
		try {
			cpds = new ComboPooledDataSource();
			cpds.setDriverClass("org.h2.Driver");            
			cpds.setJdbcUrl(JDBC_URL);
			cpds.setUser("sa");                                  
			cpds.setPassword("");            
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Team> readAllTeams() {
		List<Team> result = new ArrayList<>();
		try {
			Connection con = cpds.getConnection();
			PreparedStatement ps = con.prepareStatement(SELECT_TEAM_FROM_BL_TEAMS);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result.add(mapTeam(rs));
			}
			rs.close();
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Partie readAuftaktPartie(Team t) {
		Partie p = new Partie();
		try {
			Connection con = cpds.getConnection();
			PreparedStatement ps = con.prepareStatement(SELECT_AUFTAKTPARTIE);
			ps.setInt(1, t.getId());
			ps.setInt(2, t.getId());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				p = mapPartie(rs);
			}
			rs.close();
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public Team readTeam(String kuerzel) {
		Team result = new Team();
		try {
			Connection con = cpds.getConnection();
			PreparedStatement ps = con.prepareStatement(SELECT_TEAM_BY_KUERZEL);
			ps.setString(1, kuerzel);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result = mapTeam(rs);
			}
			rs.close();
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public List<Partie> readHeimsiegeBySpieltag() {
		List<Partie> result = new ArrayList<>();
		try {
			Connection con = cpds.getConnection();
			PreparedStatement ps = con.prepareStatement(SELECT_HEIMSIEGE);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result.add(mapPartie(rs));
			}
			rs.close();
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<Partie> readErgebnisseBySpieltagFuerTeam(Team t) {
		List<Partie> result = new ArrayList<>();
		try {
			Connection con = cpds.getConnection();
			PreparedStatement ps = con.prepareStatement(SELECT_HEIMSIEGE_FOR_TEAM);
			ps.setInt(1, t.getId());
			ps.setInt(2, t.getId());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result.add(mapPartie(rs));
			}
			rs.close();
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	protected Partie mapPartie(ResultSet rs) throws SQLException {
		Partie p = new Partie();
		Team heim = new Team();
		heim.setName(rs.getString("heim"));
		p.setHeim(heim);
		Team gast = new Team();
		gast.setName(rs.getString("gast"));
		p.setGast(gast);
		p.setToreHeim(rs.getInt("TORE_HEIM"));
		p.setToreGast(rs.getInt("TORE_GAST"));
		p.setErgebnis(Ergebnis.values()[Integer.parseInt(rs.getString("ergebnis"))]);
		return p;
	}

	private Team mapTeam(ResultSet rs) throws SQLException {
		Team t = new Team();
		t.setId(rs.getInt("TEAM_ID"));
		t.setName(rs.getString("TEAM"));
		return t;
	}

	@Override
	public void populateDb(List<Team> alleTeams, List<Partie> allePartien) {
		try {
			Connection con = cpds.getConnection();
			for (Team team : alleTeams) {
				PreparedStatement ps = con.prepareStatement(INSERT_TEAM);
				ps.setInt(1, team.getId());
				ps.setString(2, team.getName());
				ps.executeUpdate();
				ps.close();
			}
			PreparedStatement ps = con.prepareStatement(INSERT_PARTIE);
 			for (Partie partie : allePartien) {
				ps.setInt(1, partie.getId());
				ps.setInt(2, partie.getSpieltag());
				ps.setInt(3, partie.getHeim().getId());
				ps.setInt(4, partie.getGast().getId());
				ps.setInt(5, partie.getErgebnis().ordinal());
				ps.setInt(6, partie.getToreHeim());
				ps.setInt(7, partie.getToreGast());
				ps.executeUpdate();
			}
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public List<Partie> readAllPartien() {
		List<Partie> result = new ArrayList<>();
		try {
			Connection con = cpds.getConnection();
			PreparedStatement ps = con.prepareStatement(SELECT_ALL_PARTIEN);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result.add(mapPartie(rs));
			}
			rs.close();
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}


