package de.fettexplosion.persistenzdemo.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.openliga.OpenLigaDao;
import de.fettexplosion.persistenzdemo.results.ResultsProvider;

public class BundesligaImperative implements BundesligaDao {
	
	List<Team> alleTeams;
	List<Partie> allePartien;

	@Override
	public Team readTeam(String kuerzel) {
		Team result = null;
		for (Team t : alleTeams) {
			if (t.getName().toUpperCase().matches(kuerzel.toUpperCase())) {
				result = t;
			}
		}
		return result;
	}

	@Override
	public List<Team> readAllTeams() {
		return alleTeams;
	}

	@Override
	public Partie readAuftaktPartie(Team t) {
		Partie result = null;
		for (Partie p : allePartien) {
			if (p.getSpieltag() == 1 && p.getHeim().equals(t)) {
				result = p;
			}
		}
		return result;
	}

	@Override
	public List<Partie> readErgebnisseBySpieltagFuerTeam(Team t) {
		List<Partie> result = new ArrayList<>();
		for (Partie partie : allePartien) {
			if (partie.getHeim().equals(t) || partie.getGast().equals(t)) {
				result.add(partie);
			}
		}
		return result;
	}

	@Override
	public List<Partie> readHeimsiegeBySpieltag() {
		List<Partie> result = new ArrayList<>();
		for (Partie partie : allePartien) {
			if (partie.getErgebnis() == Ergebnis.heimsieg) {
				result.add(partie);
			}
		}
		return result;
	}

	@Override
	public List<Partie> readAllPartien() {
		return allePartien;
	}
	
	@Override
	public Set<Integer> readSpieltageFuerPaarung(Team t1, Team t2) {
		Set<Integer> result = new HashSet<>();
		for (Partie partie : allePartien) {
			if (istPartieVonTeam(partie, t1) && istPartieVonTeam(partie, t2)) {
				result.add(partie.getSpieltag());
			}
		}
		return result;
	}

	private boolean istPartieVonTeam(Partie p, Team t) {
		return p.getGast().equals(t) || p.getHeim().equals(t);
	}

	@Override
	public void populateDb(List<Team> alleTeams, List<Partie> allePartien) {
		ResultsProvider r = new OpenLigaDao();
		this.alleTeams = r.getAllTeams();
		this.allePartien = r.getAllPartien();
	}

}
