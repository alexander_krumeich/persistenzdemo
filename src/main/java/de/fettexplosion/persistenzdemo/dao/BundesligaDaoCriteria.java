package de.fettexplosion.persistenzdemo.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public class BundesligaDaoCriteria implements BundesligaDao {

	protected SessionFactory sessionFactory;

	@SuppressWarnings("deprecation")
	public BundesligaDaoCriteria() {
        sessionFactory = new Configuration()
        .configure()
        .addAnnotatedClass(Team.class) 
        .addAnnotatedClass(Partie.class)
        .buildSessionFactory();
	}
	
	@Override
	public Team readTeam(String kuerzel) {
		Session session = sessionFactory.openSession();
        session.beginTransaction();
        Criteria cr = session.createCriteria(Team.class);
        cr.add(Restrictions.ilike("name", kuerzel));
        Team result = (Team) cr.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return result; 

	}

	@Override
	public List<Team> readAllTeams() {
		Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
		List<Team> result = session.createCriteria(Team.class).list();
        session.close();
        return result;
	}

	@Override
	public Partie readAuftaktPartie(Team t) {
		Session session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Partie.class);
		Criterion heim = Restrictions.eq("heim", t);
		Criterion gast = Restrictions.eq("gast", t);
		LogicalExpression heimGast = Restrictions.or(heim, gast);
		Criterion spieltag = Restrictions.eq("spieltag", 1);
		cr.add(spieltag).add(heimGast);
		Partie result = (Partie) cr.uniqueResult();
		session.close();
		return result; 
	}


	@Override
	public List<Partie> readErgebnisseBySpieltagFuerTeam(Team t) {
		Session session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Partie.class);
		Criterion heim = Restrictions.eq("heim", t);
		Criterion gast = Restrictions.eq("gast", t);
		cr.add(Restrictions.or(heim, gast));
		@SuppressWarnings("unchecked")
		List<Partie> result = cr.list();
		session.close();
		return result; 
	}

	@Override
	public List<Partie> readHeimsiegeBySpieltag() {
		Session session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Partie.class).add(Restrictions.eq("ergebnis", Ergebnis.heimsieg));
		@SuppressWarnings("unchecked")
		List<Partie> result = cr.list();
		session.close();
		return result; 
	}

	@Override
	public List<Partie> readAllPartien() {
		Session session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Partie.class);
		@SuppressWarnings("unchecked")
		List<Partie> result = cr.list();
		session.close();
		return result; 
	}

	@Override
	public void populateDb(List<Team> alleTeams, List<Partie> allePartien) {
		Transaction tx = null;
		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		for (Team t : alleTeams) {
			session.save(t);
		}
		for (Partie p : allePartien) {
			session.save(p);
		}
		tx.commit();
		session.close();
	}

}
