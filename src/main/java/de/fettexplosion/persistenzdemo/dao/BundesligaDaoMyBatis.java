package de.fettexplosion.persistenzdemo.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.mapper.allteams.AllTeamsMapper;
import de.fettexplosion.persistenzdemo.mapper.spielausgang.SpielausgangMapper;
import de.fettexplosion.persistenzdemo.mapper.vereinsname.VereinsnameMapper;

/**
 * 
 */
public class BundesligaDaoMyBatis implements BundesligaDao {

	protected SqlSessionFactory sqlSessionFactory;

	public BundesligaDaoMyBatis() {
		try {
			InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Team readTeam(String kuerzel) {
		SqlSession session = sqlSessionFactory.openSession();
		Team team;
		try {
			VereinsnameMapper v = session.getMapper(VereinsnameMapper.class);
			team = v.readTeam(kuerzel);
		} finally {
			session.close();
		}
		return team;
	}

	@Override
	public List<Team> readAllTeams() {
		SqlSession session = sqlSessionFactory.openSession();
		List<Team> alleTeams;
		try {
			AllTeamsMapper at = session.getMapper(AllTeamsMapper.class);
			alleTeams = at.readAllTeams();
		} finally {
			session.close();
		}
		return alleTeams;
	}

	@Override
	public Partie readAuftaktPartie(Team t) {
		SqlSession session = sqlSessionFactory.openSession();
		Partie auftaktPartie;
		try {
			SpielausgangMapper a = session.getMapper(SpielausgangMapper.class);
			auftaktPartie = a.getAuftaktPartie(t);
		} finally {
			session.close();
		}
		return auftaktPartie;
	}

	@Override
	public List<Partie> readErgebnisseBySpieltagFuerTeam(Team t) {
		SqlSession session = sqlSessionFactory.openSession();
		List<Partie> ergebnisse;
		try {
			SpielausgangMapper hs = session.getMapper(SpielausgangMapper.class);
			ergebnisse = hs.readErgebnisseForTeam(t);
		} finally {
			session.close();
		}
		return ergebnisse;
	}

	@Override
	public List<Partie> readHeimsiegeBySpieltag() {
		SqlSession session = sqlSessionFactory.openSession();
		List<Partie> heimsiege;
		try {
			SpielausgangMapper hs = session.getMapper(SpielausgangMapper.class);
			heimsiege = hs.readHeimsiege();
		} finally {
			session.close();
		}
		return heimsiege;
	}

	@Override
	public void populateDb(List<Team> alleTeams, List<Partie> allePartien) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
			AllTeamsMapper allTeamsMapper = session.getMapper(AllTeamsMapper.class);
			for (Team team : alleTeams) {
				allTeamsMapper.addTeam(team);
			}
			SpielausgangMapper partien = session.getMapper(SpielausgangMapper.class);
			for (Partie partie : allePartien) {
				partien.addPartie(partie);
			}
			session.commit();
		} finally {
			session.close();
		}
		
	}

	@Override
	public List<Partie> readAllPartien() {
		SqlSession session = sqlSessionFactory.openSession();
		List<Partie> allePartien;
		try {
			SpielausgangMapper hs = session.getMapper(SpielausgangMapper.class);
			allePartien = hs.readAllPartien();
		} finally {
			session.close();
		}
		return allePartien;
	}

}
