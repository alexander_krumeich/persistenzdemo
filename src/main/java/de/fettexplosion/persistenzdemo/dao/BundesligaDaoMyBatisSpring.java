package de.fettexplosion.persistenzdemo.dao;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.mapper.allteams.AllTeamsMapper;
import de.fettexplosion.persistenzdemo.mapper.spielausgang.SpielausgangMapper;
import de.fettexplosion.persistenzdemo.mapper.vereinsname.VereinsnameMapper;

/**
 * 
 */
public class BundesligaDaoMyBatisSpring implements BundesligaDao {

	protected ApplicationContext ctx;
	private AllTeamsMapper allTeamsMapper;
	private SpielausgangMapper spielausgangMapper;
	private VereinsnameMapper vereinsnameMapper;

	public BundesligaDaoMyBatisSpring() {
		ctx = new ClassPathXmlApplicationContext("application-config.xml");
		allTeamsMapper = ctx.getBean(AllTeamsMapper.class);
		spielausgangMapper = ctx.getBean(SpielausgangMapper.class);
		vereinsnameMapper = ctx.getBean(VereinsnameMapper.class);
	}

	@Override
	public Team readTeam(String kuerzel) {
		return vereinsnameMapper.readTeam(kuerzel);
	}

	@Override
	public List<Team> readAllTeams() {
		return allTeamsMapper.readAllTeams();
	}

	@Override
	public Partie readAuftaktPartie(Team t) {
		return spielausgangMapper.getAuftaktPartie(t);
	}

	@Override
	public List<Partie> readErgebnisseBySpieltagFuerTeam(Team t) {
		return spielausgangMapper.readErgebnisseForTeam(t);
	}

	@Override
	public List<Partie> readHeimsiegeBySpieltag() {
		return spielausgangMapper.readHeimsiege();
	}

	@Override
	public List<Partie> readAllPartien() {
		return spielausgangMapper.readAllPartien();
	}
	
	public void setAllTeamsMapper(AllTeamsMapper allTeamsMapper) {
		this.allTeamsMapper = allTeamsMapper;
	}

	@Override
	public void populateDb(List<Team> alleTeams, List<Partie> allePartien) {
		for (Team team : alleTeams) {
			allTeamsMapper.addTeam(team);
		}
		for (Partie partie : allePartien) {
			spielausgangMapper.addPartie(partie);
		}
	}

}
