package de.fettexplosion.persistenzdemo;

import java.util.Arrays;
import java.util.List;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public class BundesligaTestWelt {
	
	public static final String STPAULI_NAME = "FC St. Pauli";
	public static final String STPAULI_ORT = "Hamburg";
	
	public static final Team vfrAalen = new Team(1,"VfR Aalen");
	public static final Team fcErzgebirgeAue = new Team(2,"FC Erzgebirge Aue");
	public static final Team herthaBsc = new Team(3,"Hertha BSC");
	public static final Team fcUnion = new Team(4,"1. FC Union Berlin");
	public static final Team eintrachtBraunschweig = new Team(5,"Eintracht Braunschweig");
	public static final Team vflBochum = new Team(6,"VfL Bochum 1848");
	public static final Team fcEnergieCottbus = new Team(7,"FC Energie Cottbus");
	public static final Team dynamoDresden = new Team(8,"SG Dynamo Dresden");
	public static final Team msvDuisburg = new Team(9,"MSV Duisburg");
	public static final Team fsvFrankfurt = new Team(10,"FSV Frankfurt 1899");
	public static final Team fcStPauli = new Team(11,"FC St. Pauli");
	public static final Team fcIngolstadt = new Team(12,"FC Ingolstadt 04");
	public static final Team fckaiserslautern = new Team(13,"1. FC Kaiserslautern");
	public static final Team effZeh = new Team(14,"1. FC Köln");
	public static final Team tsv1860Muenchen = new Team(15,"TSV München 1860");
	public static final Team scPaderborn = new Team(16,"SC Paderborn 07");
	public static final Team jahnRegensburg = new Team(17,"SSV Jahn Regensburg");
	public static final Team svSandhausen = new Team(18,"SV Sandhausen 1916");
	
	public static final List<Team> alleTeams = Arrays.asList(new Team[]{vfrAalen, fcErzgebirgeAue, herthaBsc, fcUnion, 
			eintrachtBraunschweig, vflBochum, fcEnergieCottbus, dynamoDresden, msvDuisburg, fsvFrankfurt,
			fcStPauli, fcIngolstadt, fckaiserslautern, effZeh, tsv1860Muenchen, scPaderborn, jahnRegensburg, svSandhausen
			});
	
	public static final Partie p001 = new Partie(1, 1, fcIngolstadt, fcEnergieCottbus, Ergebnis.unentschieden, 2, 2);
	public static final Partie p002 = new Partie(2, 1, fcErzgebirgeAue, fcStPauli, Ergebnis.unentschieden, 0, 0);
	public static final Partie p003 = new Partie(3, 1, herthaBsc, scPaderborn, Ergebnis.unentschieden, 2, 2);
	public static final Partie p004 = new Partie(4, 1, tsv1860Muenchen, jahnRegensburg, Ergebnis.heimsieg, 1, 0);
	public static final Partie p005 = new Partie(5, 1, vflBochum, dynamoDresden, Ergebnis.heimsieg, 2, 1);
	public static final Partie p006 = new Partie(6, 1, msvDuisburg, vfrAalen, Ergebnis.auswärtssieg, 1, 4);
	public static final Partie p007 = new Partie(7, 1, svSandhausen, fsvFrankfurt, Ergebnis.auswärtssieg, 0, 1);
	public static final Partie p008 = new Partie(8, 1, eintrachtBraunschweig, effZeh, Ergebnis.heimsieg, 1, 0);
	public static final Partie p009 = new Partie(9, 1, fckaiserslautern, fcUnion, Ergebnis.unentschieden, 3, 3);
	
	
	public static final Partie p010 = new Partie(10, 2, effZeh, svSandhausen, Ergebnis.unentschieden, 1, 1);
	public static final Partie p011 = new Partie(11, 2, vfrAalen, fckaiserslautern, Ergebnis.auswärtssieg, 1, 2);
	public static final Partie p012 = new Partie(12, 2, herthaBsc, fcErzgebirgeAue, Ergebnis.heimsieg, 3, 0);
	public static final Partie p013 = new Partie(13, 2, scPaderborn, vflBochum, Ergebnis.heimsieg, 4, 0);
	public static final Partie p014 = new Partie(14, 2, fcStPauli, fcIngolstadt, Ergebnis.unentschieden, 1, 1);
	public static final Partie p015 = new Partie(15, 2, fcUnion, eintrachtBraunschweig, Ergebnis.auswärtssieg, 0, 1);
	public static final Partie p016 = new Partie(16, 2, jahnRegensburg, msvDuisburg, Ergebnis.heimsieg, 2, 0);
	public static final Partie p017 = new Partie(17, 2, fsvFrankfurt, herthaBsc, Ergebnis.heimsieg, 3, 1);
	public static final Partie p018 = new Partie(18, 2, dynamoDresden, tsv1860Muenchen, Ergebnis.heimsieg, 3, 1);
	
	public static final Partie p019 = new Partie(19, 3, herthaBsc, jahnRegensburg, Ergebnis.heimsieg, 2, 1);
	public static final Partie p020 = new Partie(20, 3, eintrachtBraunschweig, scPaderborn, Ergebnis.heimsieg, 2, 1);
	public static final Partie p021 = new Partie(21, 3, svSandhausen, fcUnion, Ergebnis.heimsieg, 2, 0);
	public static final Partie p022 = new Partie(22, 3, msvDuisburg, dynamoDresden, Ergebnis.auswärtssieg, 1, 3);
	public static final Partie p023 = new Partie(23, 3, fcEnergieCottbus, fcStPauli, Ergebnis.heimsieg, 2, 0);
	public static final Partie p024 = new Partie(24, 3, fckaiserslautern, tsv1860Muenchen, Ergebnis.unentschieden, 0, 0);
	public static final Partie p025 = new Partie(25, 3, vflBochum, vfrAalen, Ergebnis.auswärtssieg, 1, 3);
	public static final Partie p026 = new Partie(26, 3, fcIngolstadt, fsvFrankfurt, Ergebnis.auswärtssieg, 0, 2);
	public static final Partie p027 = new Partie(27, 3, fcErzgebirgeAue, effZeh, Ergebnis.heimsieg, 2, 0);
	
	public static final List<Partie> allePartien = Arrays.asList(new Partie[]{p001, p002, p003, p004, p005, p006, p007, p008, p009,
			p010,p011,p012,p013,p014,p015,p016,p017,p018,p019,p020,p021,p022,p023,p024,p025,p026,p027});

	public static final List<Partie> alleStPauliSpiele = Arrays.asList(new Partie[]{p002, p014, p023});
	
	private BundesligaTestWelt() {
	}

}
