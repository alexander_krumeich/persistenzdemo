package de.fettexplosion.persistenzdemo.openliga;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import de.fettexplosion.persistenzdemo.bm.Ergebnis;
import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.results.ResultsProvider;
import de.msiggi.sportsdata.webservices.Matchdata;
import de.msiggi.sportsdata.webservices.Sportsdata;
import de.msiggi.sportsdata.webservices.SportsdataSoap;

public class OpenLigaDao implements ResultsProvider {

	protected final String saison;
	protected final String liga;

	protected static final Map<Integer, Team> teamCache = new HashMap<>();

	public static void main(String[] args) {
		OpenLigaDao d = new OpenLigaDao();
		System.out.println(d.getAllTeams());
		List<Partie> partien = d.getAllPartien();
		for (Partie partie : partien) {
			System.out.println(partie);
		}
	}

	public OpenLigaDao() {
		Properties p = new Properties();
		InputStream fis = getClass().getClassLoader().getResourceAsStream("liga.properties");
		try {
			p.load(fis);
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		saison = p.getProperty("cfg.spielzeit");
		liga = p.getProperty("cfg.liga");
	}

	@Override
	public List<Team> getAllTeams() {
		SportsdataSoap port = setupOpenLigaConnection();
		List<de.msiggi.sportsdata.webservices.Team> openLigaTeams = port.getTeamsByLeagueSaison(liga, saison).getTeam();
		List<Team> teams = mapTeams(openLigaTeams);
		return teams;
	}

	@Override
	public List<Partie> getAllPartien() {
		SportsdataSoap port = setupOpenLigaConnection();
		List<Matchdata> partien = port.getMatchdataByLeagueSaison(liga, saison).getMatchdata();
		List<Partie> result = mapOpenLigaData(partien);
		return result;
	}

	private List<Team> mapTeams(List<de.msiggi.sportsdata.webservices.Team> openLigaTeams) {
		List<Team> result = new ArrayList<>();
		for (de.msiggi.sportsdata.webservices.Team olTeam : openLigaTeams) {
			Team t = new Team();
			t.setId(olTeam.getTeamID());
			t.setName(olTeam.getTeamName());
			result.add(t);
			teamCache.put(t.getId(), t);
		}
		return result;
	}

	private List<Partie> mapOpenLigaData(List<Matchdata> partien) {
		List<Partie> result = new ArrayList<>();
		for (Matchdata matchdata : partien) {
			if (!matchdata.getMatchResults().getMatchResult().isEmpty()) {
				Partie p = new Partie();
				p.setHeim(teamCache.get(matchdata.getIdTeam1()));
				p.setGast(teamCache.get(matchdata.getIdTeam2()));
				p.setId(matchdata.getMatchID());
				p.setSpieltag(matchdata.getGroupOrderID());
				p.setErgebnis(mapErgebnis(matchdata));
				p.setToreHeim(matchdata.getPointsTeam1());
				p.setToreGast(matchdata.getPointsTeam2());
				result.add(p);
			}
		}
		return result;
	}

	private Ergebnis mapErgebnis(Matchdata matchdata) {
		int heimtore = matchdata.getPointsTeam1();
		int gasttore = matchdata.getPointsTeam2();
		if (heimtore > gasttore) {
			return Ergebnis.heimsieg;
		} else if (gasttore > heimtore) {
			return Ergebnis.auswärtssieg;
		} else {
			return Ergebnis.unentschieden;
		}
	}

	private SportsdataSoap setupOpenLigaConnection() {
		return new Sportsdata().getSportsdataSoap();
	}
}
