package de.fettexplosion.persistenzdemo;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;
import de.fettexplosion.persistenzdemo.dao.BundesligaDao;
import de.fettexplosion.persistenzdemo.openliga.OpenLigaDao;
import de.fettexplosion.persistenzdemo.results.ResultsProvider;

/**
 * 
 */
public class BundesligaDemo {

	private static final String DEFAULT_DAO_CLASS = "de.fettexplosion.persistenzdemo.dao.BundesligaDaoMyBatisSpring";

	BundesligaDao blDao;

	public static void main(String[] args) throws Exception {
		String theDaoClazzName = args.length >= 1 ? args[0] : DEFAULT_DAO_CLASS;
		BundesligaDemo demo = new BundesligaDemo(theDaoClazzName);
		demo.runDemoSuite();
	}

	public BundesligaDemo(String theDaoClassName) throws Exception  {
		Class<?> theDaoClazz = Class.forName(theDaoClassName);
		blDao = (BundesligaDao) theDaoClazz.newInstance();
		System.out.println("Running Demo with " + blDao.getClass());
	}
	
	public void runDemoSuite() {
		try {
			populateDatabase();
			printAllTeams();
			printAllePartien();
			printVereinsname();
			printAuftaktPartieForTeam();
			printHeimsiege();
			printErgebnisseFuerTeam();
			printSpieltageFuerPaarung();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void printAllePartien() {
		System.out.println("Alle Partien");
		System.out.println("==============");
		List<Partie> allePartien = blDao.readAllPartien();
		for (Partie partie : allePartien) {
			System.out.println(partie);
		}
		System.out.println();
		
	}

	public void populateDatabase() {
		System.out.println("Befülle Datenbank");
		System.out.println("=================");
		ResultsProvider r = new OpenLigaDao();
		List<Team> alleTeams = r.getAllTeams();
		List<Partie> allePartien = r.getAllPartien();
		blDao.populateDb(alleTeams, allePartien);

	}

	public void printHeimsiege() throws SQLException {
		System.out.println("Alle Heimsiege");
		System.out.println("==============");
		List<Partie> allHeimsiege = blDao.readHeimsiegeBySpieltag();
		for (Partie partie : allHeimsiege) {
			System.out.println(partie);
		}
		System.out.println();
	}

	public void printErgebnisseFuerTeam() throws SQLException {
		System.out.println("Alle St. Pauli Ergebnisse");
		System.out.println("=========================");
		Team stPauli = blDao.readTeam("FC St. Pauli");
		List<Partie> alleErgebnisse = blDao.readErgebnisseBySpieltagFuerTeam(stPauli);
		for (Partie partie : alleErgebnisse) {
			System.out.println(partie);
		}
		System.out.println();
	}

	public void printAllTeams() throws SQLException {
		System.out.println("Alle Teams");
		System.out.println("==========");
		List<Team> allTeams = blDao.readAllTeams();
		for (Team team : allTeams) {
			System.out.println(team);
		}
		System.out.println();
	}

	public void printVereinsname() throws SQLException {
		System.out.println("Vereinsname");
		System.out.println("===========");
		Team stPauli = blDao.readTeam("FC St. Pauli");
		System.out.println(stPauli);
		System.out.println();
	}

	public void printAuftaktPartieForTeam() throws SQLException {
		System.out.println("Auftaktpartie");
		System.out.println("=============");
		Team stPauli = blDao.readTeam("FC St. Pauli");
		Partie auftaktPartie = blDao.readAuftaktPartie(stPauli);
		System.out.println(auftaktPartie);
		System.out.println();
	}
	
	public void printSpieltageFuerPaarung() throws SQLException {
		System.out.println("Spieltage für Paarung");
		System.out.println("=====================");
		Team stPauli = blDao.readTeam("FC St. Pauli");
		Team fortuna = blDao.readTeam("Fortuna Düsseldorf");
		Set<Integer> spieltage = blDao.readSpieltageFuerPaarung(stPauli, fortuna);
		for (Integer spieltag : spieltage) {
			System.out.println("Spieltag für " + stPauli.getName() + " gegen " + fortuna.getName() + ": " + spieltag);
		}
		System.out.println();
		
	}
	
}
