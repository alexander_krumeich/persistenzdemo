package de.fettexplosion.persistenzdemo.mapper.allteams;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public interface AllTeamsMapper {

	@Select("select TEAM_ID,TEAM from teams")
	@Results(value = {
		@Result(property="name", column="TEAM"),
		@Result(property="id", column="TEAM_ID")
	})
	@Options(useCache=true)
	List<Team> readAllTeams();
	
	@Insert("INSERT INTO teams (TEAM_ID, TEAM) VALUES (#{team.id}, #{team.name})")
	void addTeam(@Param("team") Team team);
	
}
