package de.fettexplosion.persistenzdemo.mapper.vereinsname;

import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public interface VereinsnameMapper {
	
	Team readTeam(String kuerzel);

}
