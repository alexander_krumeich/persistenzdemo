package de.fettexplosion.persistenzdemo.mapper.spielausgang;

import java.util.List;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;

/**
 * 
 */
public interface SpielausgangMapper {

	void addPartie(Partie p);
	
	List<Partie> readHeimsiege();
	
	List<Partie> readErgebnisseForTeam(Team t);
	
	List<Partie> readAllPartien();
	
	Partie getAuftaktPartie(Team t);
	
}
