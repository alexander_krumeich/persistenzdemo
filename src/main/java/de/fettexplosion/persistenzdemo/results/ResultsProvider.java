package de.fettexplosion.persistenzdemo.results;

import java.util.List;

import de.fettexplosion.persistenzdemo.bm.Partie;
import de.fettexplosion.persistenzdemo.bm.Team;

public interface ResultsProvider {

	List<Partie> getAllPartien();

	List<Team> getAllTeams();

}
