CREATE SCHEMA IF NOT EXISTS BL;
SET SCHEMA BL;


DROP TABLE teams IF EXISTS;
create table teams(TEAM_ID INTEGER, TEAM VARCHAR(256));

DROP TABLE partien IF EXISTS;
create table partien(PARTIE_ID INTEGER, SPIELTAG INTEGER, HEIM VARCHAR(256), GAST VARCHAR(256), ERGEBNIS INTEGER, TORE_HEIM INTEGER, TORE_GAST INTEGER);
