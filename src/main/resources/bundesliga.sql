DROP TABLE teams IF EXISTS;
create table teams(TEAM_ID INTEGER, NR INTEGER, TEAM VARCHAR(256), ORT VARCHAR(256));
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (1,1,'VfR Aalen','Aalen');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (2,2,'FC Erzgebirge Aue','Aue');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (3,3,'Hertha BSC','Berlin');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (4,4,'1. FC Union Berlin','Berlin');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (5,5,'Eintracht Braunschweig','Braunschweig');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (6,6,'VfL Bochum 1848','Bochum');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (7,7,'FC Energie Cottbus','Cottbus');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (8,8,'SG Dynamo Dresden','Dresden');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (9,9,'MSV Duisburg','Duisburg');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (10,10,'FSV Frankfurt 1899','Frankfurt');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (11,11,'FC St. Pauli','Hamburg');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (12,12,'FC Ingolstadt 04','Ingolstadt');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (13,13,'1. FC Kaiserslautern','Kaiserslautern');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (14,14,'1. FC Köln','Köln');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (15,15,'TSV München 1860','München');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (16,16,'SC Paderborn 07','Paderborn');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (17,17,'SSV Jahn Regensburg','Regensburg');
INSERT INTO teams(TEAM_ID, NR, TEAM, ORT) VALUES (18,18,'SV Sandhausen 1916','Sandhausen');

DROP TABLE partien IF EXISTS;
create table partien(PARTIE_ID INTEGER, SPIELTAG INTEGER, HEIM VARCHAR(256), GAST VARCHAR(256), ERGEBNIS INTEGER, TORE_HEIM INTEGER, TORE_GAST INTEGER);

INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (1,1,12,7,0,2,2);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (2,1,2,11,0,0,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (3,1,3,16,0,2,2);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (4,1,15,17,1,1,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (5,1,6,8,1,2,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (6,1,9,1,2,1,4);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (7,1,18,10,0,1,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (8,1,5,14,1,1,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (9,1,13,4,0,3,3);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (10,2,14,18,0,1,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (11,2,1,13,2,1,2);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (12,2,3,2,1,3,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (13,2,16,6,1,4,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (14,2,11,12,0,1,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (15,2,4,5,2,0,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (16,2,17,9,1,2,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (17,2,10,3,1,3,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (18,2,8,15,0,2,2);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (19,3,3,17,1,2,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (20,3,5,16,1,2,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (21,3,18,4,1,2,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (22,3,9,8,2,1,3);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (23,3,7,11,1,2,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (24,3,13,15,0,0,0);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (25,3,6,1,2,0,1);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (26,3,12,10,2,0,2);
INSERT INTO partien(PARTIE_ID, SPIELTAG, HEIM, GAST, ERGEBNIS, TORE_HEIM, TORE_GAST) VALUES (27,3,2,14,1,2,0);
